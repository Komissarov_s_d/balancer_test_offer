from threading import Event
import logs
import tmp
import config
import http
import httpx
import json
import os

def check_health_server():
    while True:
        try:
            for i in range(len(tmp.status_hots)):
                url = f'{tmp.status_hots[i].get("host")}/health'
                try:
                    respons = httpx.post(url=url)
                    if respons.status_code == http.HTTPStatus.OK:
                        tmp.status_hots[i]["status"] = True
                    else:
                        tmp.status_hots[i]["status"] = False
                        tmp.status_hots[i]["count"] = 0
                except:
                    tmp.status_hots[i]["status"] = False
                    tmp.status_hots[i]["count"] = 0
        except Exception as e:
            logs.save_log_error(e)
        Event().wait(config.CHECK_HEALTH_SERVER_TIME)


def get_host_id_to_redirect():
    number_of_requests = None
    host_id = None
    for i in range(len(tmp.status_hots)):
        if tmp.status_hots[i].get("status"):
            if number_of_requests is None:  # an extra check when processing 1 object, but removes the error to go outside the array
                number_of_requests = tmp.status_hots[i].get("count")
                host_id = i
            if tmp.status_hots[i].get("count") < number_of_requests:
                number_of_requests = tmp.status_hots[i].get("count")
                host_id = i
    if host_id is None:
        logs.save_log_critical("no live hosts")
        return -1
    return host_id


def load_config_hosts():
    try:
        with open('app/config_hosts.json', "r") as file_object:
            data = file_object.read()
            arr_host = json.loads(data)
            for item in arr_host:
                tmp.status_hots.append({
                    "host": item,
                    "count": 0,
                    "status": False
                })
    except Exception as e:
        logs.save_log_error(e)


def statistics():
    try:
        app_name = os.environ["APP"]
        if app_name:
            return {app_name: tmp.status_hots}
    except:
        return tmp.status_hots


def deamon_save_log():
    while True:
        st = statistics()
        logs.save_log_info(st)
        Event().wait(config.CHECK_SAVE_LOG_TIME)
