from fastapi import FastAPI, Request, HTTPException
import threading
import uvicorn
import tmp
import httpx
import balancer_func
import config
import logs

app = FastAPI()

@app.get("/{path:path}")
async def query(path: str):
    host_id = balancer_func.get_host_id_to_redirect()
    if host_id == -1:
        raise HTTPException(status_code=500, detail="The service fell in battle :(")
    tmp.status_hots[host_id]["count"] = tmp.status_hots[host_id].get("count") + 1
    url = f'{tmp.status_hots[host_id].get("host")}/{path}'
    async with httpx.AsyncClient() as client:
        response = await client.get(url)
        tmp.status_hots[host_id]["count"] = tmp.status_hots[host_id].get("count") - 1
        return response.content, response.status_code


@app.post("/statistics")
async def statistics():
    return balancer_func.statistics()


if __name__ == '__main__':
    logs.init_logger()
    balancer_func.load_config_hosts()
    thread_health = threading.Thread(target=balancer_func.check_health_server)
    thread_health.start()
    thread_logs = threading.Thread(target=balancer_func.deamon_save_log)
    thread_logs.start()
    uvicorn.run(app, host=config.HOST, port=config.PORT)
