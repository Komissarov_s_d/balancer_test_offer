import logging
import tmp

def init_logger():
    tmp.logger = logging.getLogger()
    l_format = logging.Formatter('%(asctime)s - %(filename)s - %(funcName)s - %(message)s')
    l_handler = logging.FileHandler('logger.log')
    l_handler.setFormatter(l_format)
    l_handler.setLevel(logging.INFO)
    tmp.logger.addHandler(l_handler)


def save_log_info(msg):
    tmp.logger.warning(msg=str(msg))


def save_log_error(msg):
    tmp.logger.error(msg=str(msg))


def save_log_critical(msg):
    tmp.logger.critical(msg=str(msg))
